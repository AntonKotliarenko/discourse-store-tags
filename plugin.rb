# frozen_string_literal: true

# name: DiscourseStoreTags
# about: Extend tags with images and unescaped names
# version: 0.1
# authors: tohakotliarenko
# url: https://github.com/tohakotliarenko

# register_asset 'stylesheets/common/discourse-store-tags.scss'
# register_asset 'stylesheets/desktop/discourse-store-tags.scss'
# register_asset 'stylesheets/mobile/discourse-store-tags.scss'
register_asset 'javascripts/discourse/helpers/discourse-tags-customized.js.es6'
register_asset 'javascripts/discourse/lib/render-tag-customized.js.es6'
register_asset 'javascripts/discourse/lib/render-tags-customized.js.es6'
register_asset 'javascripts/discourse/templates/list/topic-list-item.raw.hbs'
register_asset 'javascripts/discourse/templates/components/topic-category.hbs'

enabled_site_setting :discourse_store_tags_enabled

PLUGIN_NAME ||= 'DiscourseStoreTag'

load File.expand_path('lib/discourse-store-tags/engine.rb', __dir__)

after_initialize do
  # https://github.com/discourse/discourse/blob/master/lib/plugin/instance.rb
end
