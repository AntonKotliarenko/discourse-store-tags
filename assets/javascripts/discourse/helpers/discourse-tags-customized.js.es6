import { registerUnbound } from "discourse-common/lib/helpers";
import renderTagsCustomized from "./../lib/render-tags-customized";

export default registerUnbound("discourse-tags-customized", function(topic, params) {
  return new Handlebars.SafeString(renderTagsCustomized(topic, params));
});
