# DiscourseStoreTags

DiscourseStoreTags is a plugin that extends DiscourseTags with images and unescaped names

## Installation

Follow [Install a Plugin](https://meta.discourse.org/t/install-a-plugin/19157)
how-to from the official Discourse Meta, using `git clone https://github.com/pricemate-com/anton-discourse-image-tags.git`
as the plugin command.

## Usage
`PluginStore.set(
        "discourse-store-tags",
        TOPIC_ID,
        {
          "TAG_ID": { image_url: "", full_name: "" },
          "TAG_ID_2": { image_url: "", full_name: "" }
        }
)`

## Feedback

If you have issues or suggestions for the plugin, please bring them up on
[Discourse Meta](https://meta.discourse.org).
