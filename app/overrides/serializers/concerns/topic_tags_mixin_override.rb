# frozen_string_literal: true

module ::TopicTagsMixin
  module DiscourseStoreTagsInstanceMethods
    def tags_extra_data
      tags_extra_data = TagsStore.data_for_topic(topic.id)
      tags = topic.tags.map do |tag|
        if tags_extra_data[tag.id.to_s].present?
          { tag.name => tags_extra_data[tag.id.to_s] }
        else
          {}
        end
      end.inject(:merge)

      unless scope.is_staff?
        scope.hidden_tag_names.each do |hidden_tag_name|
          next unless tags.key?(hidden_tag_name)
          tags.delete(hidden_tag_name)
        end
      end

      tags
    end
  end
  prepend DiscourseStoreTagsInstanceMethods

  module DiscourseStoreTagsClassMethods
    def included(klass)
      super(klass)
      klass.attributes :tags_extra_data
    end
  end
  singleton_class.prepend DiscourseStoreTagsClassMethods
end
