# frozen_string_literal: true

class TagsStore
  class << self
    def data_for_topic(topic_id)
      PluginStore.get("discourse-store-tags", topic_id) || {}
    end

    def set_data_for_topic(topic_id, data)
      PluginStore.set("discourse-store-tags", topic_id, data)
    end

    def remove_data_for_topic(topic_id)
      PluginStore.remove("discourse-store-tags", topic_id)
    end
  end
end
