# frozen_string_literal: true

module DiscourseStoreTag
  class Engine < ::Rails::Engine
    engine_name "DiscourseStoreTag".freeze
    isolate_namespace DiscourseStoreTag

    config.after_initialize do
      Dir.glob(Engine.root + "app/overrides/**/*_override*.rb").each do |c|
        require_dependency(c)
      end
      require_dependency(Engine.root + "lib/tags_store.rb")
    end
  end
end
