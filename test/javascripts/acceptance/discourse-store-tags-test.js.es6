import { acceptance } from "helpers/qunit-helpers";

acceptance("DiscourseStoreTags", { loggedIn: true });

test("DiscourseStoreTags works", async assert => {
  await visit("/admin/plugins/discourse-store-tags");

  assert.ok(false, "it shows the DiscourseStoreTags button");
});
